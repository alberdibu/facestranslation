import argparse
import ast
from pathlib import Path
import re
import os
import cv2
import subprocess
from distutils.dir_util import copy_tree
import shutil
from moviepy.video.io.ffmpeg_tools import ffmpeg_extract_subclip
import math
from pytube import YouTube


class FFmpeg:
    def __init__(self, ffmpeg_bin='ffmpeg'):
        self.ffmpeg_bin = ffmpeg_bin

    def extract_segment(self, source, destination, start, stop, timeout=None):
        args = ["-i", source,
                "-ss", str(start),
                "-to", str(stop),
                "-f", "wav", destination]
        self._run_ffmpeg(args, destination=destination, timeout=timeout)


    def _run_ffmpeg(self, args, destination=None, timeout=None):
        cmd = [self.ffmpeg_bin, "-y"] + args

        with subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE) as proc:
            try:
                stdout, stderr = proc.communicate(timeout=timeout)
                if proc.returncode != 0:
                    raise RuntimeError("ffmpeg failed:\n\n{}".format(stderr.decode("utf-8")))
            except subprocess.TimeoutExpired as e:
                if destination:
                    try:
                        os.remove(destination)
                    except OSError:
                        pass
                raise

        return stdout, stderr

def parse_segment_file(path, fps):
    path = Path(path)

    with path.open('r') as f:
        segment_desc = f.read().strip()

    header, frames = segment_desc.split("\n\n")
    start, stop = _get_segment_boundaries(frames, fps)
    return start, stop

def _get_segment_boundaries(frames, fps):
    def _get_frame_seconds(line):
        frame = int(line.split("\t")[0])
        return frame / fps
    lines = frames.split("\n")
    return _get_frame_seconds(lines[1]), _get_frame_seconds(lines[-1])


def sanitize_filename(s):
    return re.sub(r'(?u)[^-\w.]', '_', s)


def download_and_process(args):

    dataset_dir = "data"
    if not os.path.exists(dataset_dir):
        os.mkdir(dataset_dir)

    dataset_processed = "preprocessed"
    if not os.path.exists(dataset_processed):
        os.mkdir(dataset_processed)

    idx_preprocess = 0

    path = Path("txt")
    for path2 in path.iterdir():

        ids = str(path2).split("/")[-1]

        if not os.path.exists(dataset_dir+"/"+ids):
            os.mkdir(dataset_dir+"/"+ids)

        for pathId in path2.iterdir():

            idsY = str(pathId).split("/")[-1]

            if not os.path.exists(dataset_dir+"/"+ids+"/"+idsY):
                os.mkdir(dataset_dir+"/"+ids+"/"+idsY)

            if not os.path.exists(dataset_dir+"/"+ids+"/"+idsY+"/txt"):
                os.mkdir(dataset_dir+"/"+ids+"/"+idsY+"/txt")

            if not os.path.exists(dataset_dir+"/"+ids+"/"+idsY+"/wav"):
                os.mkdir(dataset_dir+"/"+ids+"/"+idsY+"/wav")

            if not os.path.exists(dataset_dir+"/"+ids+"/"+idsY+"/vid"):
                os.mkdir(dataset_dir+"/"+ids+"/"+idsY+"/vid")

            copy_tree(pathId, dataset_dir+"/"+ids+"/"+idsY+"/txt")

            # If download is activated, download video and audio
            if args["download_dataset"]:
                '''cmd1 = ["youtube-dl",
                       "--extract-audio",
                       "--audio-format", "wav",
                       "--audio-quality", "1",
                       "--output", dataset_dir+"/"+ids+"/"+idsY+"/"+idsY+".wav", idsY]

                cmd2 = ["youtube-dl",
                       "-f", "best",
                       "--output", dataset_dir+"/"+ids+"/"+idsY+"/"+idsY+".mp4",
                       "http://www.youtube.com/watch?v="+idsY]'''

                '''print('http://youtube.com/watch?v='+idsY)
                yt = YouTube('http://youtube.com/watch?v='+idsY)
                yt.streams.first().download(dataset_dir+"/"+ids+"/"+idsY+"/"+idsY+".mp4")'''

                run = subprocess.run(cmd1, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                #run = subprocess.run(cmd2, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

            # Get video FPS
            video = cv2.VideoCapture(dataset_dir+"/"+ids+"/"+idsY+"/"+idsY+".mp4")
            fps = math.ceil(video.get(cv2.CAP_PROP_FPS))
            #width  = video.get(3)
            #height = video.get(4)
            if fps == 0:
                shutil.rmtree(dataset_dir+"/"+ids+"/"+idsY)
                continue

            width  = 1080
            height = 720

            video.release()
     
            for pathTxt in pathId.iterdir():

                idsT = str(pathTxt).split("/")[-1]

                start, stop = parse_segment_file(pathTxt, fps)

                ffmpeg = FFmpeg()

                #try:

                #ffmpeg.extract_segment(dataset_dir+"/"+ids+"/"+idsY+"/"+idsY+".wav", dataset_dir+"/"+ids+"/"+idsY+"/wav/"+idsT.split(".")[0]+".wav", start=start, stop=stop, timeout=300)
                #ffmpeg_extract_subclip(dataset_dir+"/"+ids+"/"+idsY+"/"+idsY+".mp4", start, stop, targetname=dataset_dir+"/"+ids+"/"+idsY+"/vid/"+idsT.split(".")[0]+".mp4")
            
                txt_frames = []
                txt_bbox = []
                with open(pathTxt) as txt_file:
                    content = txt_file.readlines()

                    for i, line in enumerate(content):
                        if i<7:
                            continue
                        line = line.replace('\n', ' ').replace('\r', '').replace('\t', '').split(" ")
                        txt_frames.append(int(line[0]))
                        txt_bbox.append([float(line[1]), float(line[2]), float(line[3]), float(line[4])])

                if not os.path.exists(dataset_processed+"/"+str(idx_preprocess).zfill(10)):
                    os.mkdir(dataset_processed+"/"+str(idx_preprocess).zfill(10))

                video = cv2.VideoCapture(dataset_dir+"/"+ids+"/"+idsY+"/"+idsY+".mp4")

                frame_id = 0
                while(video.isOpened()):

                    ret, frame = video.read()
                    if ret == False:
                        break

                    if frame_id in txt_frames:

                        idx_bbox = txt_frames.index(frame_id)
                        print(idx_bbox, len(txt_bbox), txt_bbox[idx_bbox])
                        cut_frame = frame[int(txt_bbox[idx_bbox][1]*height):int((txt_bbox[idx_bbox][1]+txt_bbox[idx_bbox][3])*height), int(txt_bbox[idx_bbox][0]*width):int((txt_bbox[idx_bbox][0]+txt_bbox[idx_bbox][2])*width)]
                        cv2.imshow("Face", cut_frame)
                        cv2.waitKey(0)

                    elif frame_id>txt_frames[-1]:
                        break

                    #cv2.imwrite(dataset_processed+"/"+str(idx_preprocess).zfill(10)+"/"+str(frame_id).zfill(5)+"_frame.jpg", frame)



                    #cv2.imwrite(dataset_processed+"/"+str(idx_preprocess).zfill(10)+"/"+str(frame_id).zfill(5)+"_frame.jpg", frame)
                    
                    frame_id+=1
             
                video.release()


                idx_preprocess += 1



                '''except:
                    shutil.rmtree(dataset_dir+"/"+ids+"/"+idsY)
                    shutil.rmtree(dataset_processed+"/"+str(idx_preprocess).zfill(10))
                    break'''






if __name__ == "__main__":

    # Construct and Parse input arguments
    ap = argparse.ArgumentParser()
    ap.add_argument('-download','--download_dataset', default = True, type = ast.literal_eval,
                    help = 'Download or not the video and audio from the txt files')
    ap.add_argument('-landmarks1','--extractLandmarks', default = True, type = ast.literal_eval,
                    help = 'whether to extract Landmark from video or not')
    ap.add_argument('-landmarks2','--extractLandmarksImage', default = True, type = ast.literal_eval,
                    help = 'whether to extract Landmarks Image from video or not')

    args = vars(ap.parse_args())

    download_and_process(args)