from faces_service.face_detection import face_detection
from faces_service.face_process import face_process

import multiprocessing


def data_extraction(video_path):
	face_detection(video_path=video_path)

p = multiprocessing.Process(target=data_extraction, args=("example.mp4",))
p.start()

