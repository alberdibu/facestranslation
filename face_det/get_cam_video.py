from keras.preprocessing.image import img_to_array
import imutils
import cv2
from keras.models import load_model
import numpy as np
import tensorflow as tf

session_config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
sess = tf.Session(config=session_config)

detection_model_path = 'haarcascade_files/haarcascade_frontalface_default.xml'
face_detection = cv2.CascadeClassifier(detection_model_path)


#feelings_faces = []
#for index, emotion in enumerate(EMOTIONS):
   # feelings_faces.append(cv2.imread('emojis/' + emotion + '.png', -1))

# starting video streaming
cv2.namedWindow('Your Face')
camera = cv2.VideoCapture(0)
while True:
    frame = camera.read()[1]
    ho, wo, _ = frame.shape
    #reading the frame
    frame_res = imutils.resize(frame,width=300)
    h, w, _ = frame_res.shape
    gray = cv2.cvtColor(frame_res, cv2.COLOR_BGR2GRAY)
    faces = face_detection.detectMultiScale(gray,scaleFactor=1.1,minNeighbors=5,minSize=(30,30),flags=cv2.CASCADE_SCALE_IMAGE)
    
    frameClone = frame.copy()
    if len(faces) > 0:
        faces = sorted(faces, reverse=True,
        key=lambda x: (x[2] - x[0]) * (x[3] - x[1]))[0]
        (fX, fY, fW, fH) = faces

        roi = gray[fY:fY + fH, fX:fX + fW]

        ys = int(fY*(ho/h))
        yi = int((fY + fH)*(ho/h))
        xs = int(fX*(ho/h))
        xi = int((fX + fW)*(ho/h))

        cv2.rectangle(frameClone, (xs, ys), (xi, yi), (0, 0, 255), 2)

    cv2.imshow('your_face', frameClone)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

camera.release()
cv2.destroyAllWindows()
