import cv2
import json
import os

video_path = "example.mp4"
json_path = "example"

with open(json_path+"_faces_process.json", 'r') as outfile:
    json_data = json.load(outfile)

with open(json_path+"_faces_annotator.json", 'r') as outfile:
	json_annotator = json.load(outfile)


cam = cv2.VideoCapture(video_path)

ind_frame = 0
ind_anno = 0
len_det = len(json_annotator["frames"])
ind_folder = 0
while True:

	ret_val, frame = cam.read()

	if not ret_val or len_det==ind_anno:
		break

	ind_frame += 1

	if ind_frame==json_annotator["frames"][ind_anno][0]:

		if not os.path.exists("data/"+json_path):
			os.mkdir("data/"+json_path)

		if not os.path.exists("data/"+json_path+"/"+str(ind_anno).zfill(3)):
			os.mkdir("data/"+json_path+"/"+str(ind_anno).zfill(3))

	if ind_frame>=json_annotator["frames"][ind_anno][0] and ind_frame<=json_annotator["frames"][ind_anno][1]:

		for ids in list(json_data["faces"][ind_frame].keys()):
			bbox = json_data["faces"][ind_frame][ids]
			cut_face = frame[bbox["y"]:bbox["y"]+bbox["h"], bbox["x"]:bbox["x"]+bbox["w"]]
			cv2.imwrite("data/"+json_path+"/"+str(ind_anno).zfill(3)+"/"+str(ind_folder).zfill(4)+".jpg", cut_face)
			cv2.imshow("Face", cut_face)
			cv2.waitKey(10)

		ind_folder += 1


	if ind_frame==json_annotator["frames"][ind_anno][1]:
		ind_anno += 1
		ind_folder = 0

