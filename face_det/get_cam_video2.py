import numpy as np
import time
import cv2
import random
from pathlib import Path
from faces_service.wide_resnet import WideResNet
import json
import os

from keras.models import load_model
from keras.utils.data_utils import get_file

from faces_service.mark_detector import MarkDetector

import multiprocessing

import tensorflow as tf
graph = tf.get_default_graph()


def read_frames(video="example"):

    path = "data/"+video

    data_dirs = {}

    for root, dirs, files in os.walk(path):

        dirs.sort()

        for dire in dirs:
            data_dirs[path+"/"+dire] = []

    for path2 in list(data_dirs.keys()):
        for root, dirs, files in os.walk(path2):
            for file in files:
                data_dirs[path2].append(file)

        data_dirs[path2].sort()

    return data_dirs


def face_detection(video_path=None):
    from multiprocessing import Process, Queue
    global graph

    videos = read_frames()
    max_vid = len(list(videos.keys()))

    if not os.path.exists("data/cam_faces"):
        os.mkdir("data/cam_faces")

    with graph.as_default():

        draw_detections = True
        
        # initialize the video stream and allow the cammera sensor to warmup
        cam = cv2.VideoCapture(video_path)

        # landmark detector
        mark_detector = MarkDetector()

        # Params
        frame_n = -1

        def get_face(detector, img_queue, box_queue):
            while True:
                image = img_queue.get()
                if image[1]:
                    break
                box = detector.extract_cnn_facebox(image[0])
                box_queue.put(box)

        frame_ant = None

        print("Starting Faces Detection Process")

        rep_vid = False
        ind_vid = 1
        ind_vid_frame = 0
        save_counter = 0
        store = False

        while True:

            ret_val, frame = cam.read()

            if not ret_val:
                img_queue.put([frame_ant, True])
                break

            frame_ant = frame.copy()

            if frame_n == -1:
                img_queue = Queue()
                box_queue = Queue()
                #kill_queue = Queue()
                img_queue.put([frame, False])
                box_process = Process(target=get_face, args=(mark_detector, img_queue, box_queue))
                box_process.start()

            else:
                img_queue.put([frame, False])

            frame_n += 1
            # Face detector
            faceboxes = box_queue.get()

            boxToDraw = []
            if faceboxes is not None:
                if len(faceboxes)>0:
                    boxToDraw = faceboxes[0]

                    cv2.rectangle(frame_ant, (boxToDraw[0], boxToDraw[1]), (boxToDraw[2], boxToDraw[3]), (0, 0, 255), 2)

            cv2.imshow("Frames", frame_ant)

            if rep_vid:
                if ind_vid_frame < len(vid_frames):
                    cv2.imshow("Video", cv2.imread(path_vid+"/"+vid_frames[ind_vid_frame]))
                    ind_vid_frame += 1

                else:
                    ind_vid_frame = 0
                    rep_vid = False

            key = cv2.waitKey(1) & 0xFF
            if key == ord('q'):
                img_queue.put([frame_ant, True])
                break

            if key == ord('v'):
                rep_vid = True
                vid_frames = videos[list(videos.keys())[ind_vid-1]]
                path_vid = list(videos.keys())[ind_vid-1]
                print("Video length is " + str(len(vid_frames)) + " frames")

            if key == ord('n'):
                print("Next Video")
                ind_vid += 1
                if ind_vid > max_vid:
                    img_queue.put([frame_ant, True])
                    break

            if key == ord('r'):
                store = True

            if store:


                if not os.path.exists("data/cam_faces/"+path_vid.split("/")[-1]):
                    os.mkdir("data/cam_faces/"+path_vid.split("/")[-1])

                if faceboxes is not None and save_counter<len(vid_frames):

                    if len(faceboxes)==0:
                        store = False
                        save_counter = 0
                        print("Failing extraxting consecutive frames!")
                        cv2.destroyAllWindows()

                    else:
                        cut_face = frame[boxToDraw[1]:boxToDraw[3], boxToDraw[0]:boxToDraw[2]]
                        cv2.imshow("Face cut", cut_face)
                        cv2.imwrite("data/cam_faces/"+path_vid.split("/")[-1]+"/"+str(save_counter).zfill(4)+".jpg", cut_face)
                        save_counter += 1

                else:
                    store = False
                    save_counter = 0
                    print("Failing extraxting consecutive frames!")
                    cv2.destroyAllWindows()

                    filelist = [ f for f in os.listdir("data/cam_faces/"+path_vid.split("/")[-1]) if f.endswith(".jpg")]
                    for f in filelist:
                        os.remove(os.path.join(mydir, f))

                if save_counter == len(vid_frames):
                    store = False
                    save_counter = 0
                    print("Frames stored!")
                    cv2.destroyAllWindows()
                
        cv2.destroyAllWindows()
        cam.release()

        print("Faces extraction Finished!")


face_detection(0)