#!/usr/bin/env python
# coding: utf-8

# This jupyter notebook contains demo code for:
# - loading a model and using it to 
# - drive one or more source frames with a set of driving frames
# - modifying the embedded face to perform video editing for both the dragon tattoo and Harry Potter scar

# In[51]:


import matplotlib.pyplot as plt
import numpy as np
import torch.nn as nn
import os
import torch
from PIL import Image
from torch.autograd import Variable
from UnwrappedFace import UnwrappedFaceWeightedAverage, UnwrappedFaceWeightedAveragePose
import torchvision
from torchvision.transforms import ToTensor, Compose, Scale


# In[52]:


def run_batch(source_images, pose_images):
    return model(pose_images, *source_images)


# In[53]:


BASE_MODEL = './release_models/' # Change to your path
state_dict = torch.load(BASE_MODEL + 'x2face_model_forpython3.pth')

model = UnwrappedFaceWeightedAverage(output_num_channels=2, input_num_channels=3, inner_nc=128)
model.load_state_dict(state_dict['state_dict'])
model = model.cuda()

model = model.eval()


# **Code for driving with another set of frames**

# In[54]:


driver_path = 'examples/Taylor_Swift/1.6/nuBaabkzzzI/'
source_path = 'examples/Taylor_Swift/1.6/vBgiDYBCuxY/'
#source_path = 'examples/yo/frames/'
#driver_path = 'examples/Taylor_Swift/1.6/nuBaabkzzzI/'

driver_imgs = [driver_path + d for d in sorted(os.listdir(driver_path))][0:16] # 8 driving frames
source_imgs  = [source_path + d for d in sorted(os.listdir(source_path))][0:8] # 3 source frames


# In[55]:


from io import BytesIO
from PIL import Image, ImageFile
import cv2

ImageFile.LOAD_TRUNCATED_IMAGES = True

def load_img(file_path):
    
    img = cv2.imread(file_path)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    img = Image.fromarray(img)
    transform = Compose([Scale((256,256)), ToTensor()])
    return Variable(transform(img)).cuda()


# In[56]:


# Driving the source image with the driving sequence
source_images = []
for img in source_imgs:
    source_images.append(load_img(img).unsqueeze(0).repeat(len(driver_imgs), 1, 1, 1))
    
driver_images = None
for img in driver_imgs:
    if driver_images is None:
        driver_images = load_img(img).unsqueeze(0)
    else:
        driver_images = torch.cat((driver_images, load_img(img).unsqueeze(0)), 0)

# Run the model for each
with torch.no_grad():
    result = run_batch(source_images, driver_images)
result = result.clamp(min=0, max=1)
img = torchvision.utils.make_grid(result.cpu().data)
    
# Visualise the results
fig_size = plt.rcParams["figure.figsize"]
fig_size[0] = 24.
fig_size[1] = 24.
plt.rcParams["figure.figsize"] = fig_size
plt.axis('off')

result_images = img.permute(1,2,0).numpy()
driving_images = torchvision.utils.make_grid(driver_images.cpu().data).permute(1,2,0).numpy()
print("The results is: ")
plt.imshow(np.vstack((result_images, driving_images)))
plt.show()


