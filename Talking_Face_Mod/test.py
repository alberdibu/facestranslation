#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 19 09:03:06 2019

@author: gatvprojects
"""

# Imports
from Options_all import BaseOptions
from torch.utils.data import DataLoader
from util import util
from util.visualizer import Visualizer
import os
import time
import ntpath
import cv2

# Manage input args
opt = BaseOptions().parse()

# If test is video, image or audio load Dataloaders and Models
if opt.test_type == 'video' or opt.test_type == 'image':
    import Test_Gen_Models.Test_Video_Model_2 as Gen_Model
    from Dataloader.Test_load_video_2 import Test_VideoFolder
elif opt.test_type == 'audio':
    import Test_Gen_Models.Test_Audio_Model as Gen_Model
    from Dataloader.Test_load_audio import Test_VideoFolder
else:
    raise('test type select error')
    
    
opt.nThreads = 1   # test code only supports nThreads = 1
opt.batchSize = 1  # test code only supports batchSize = 1
opt.sequence_length = 1

model = Gen_Model.GenModel(opt)

start_epoch = opt.start_epoch
visualizer = Visualizer(opt)

# Find the checkpoint's path name without the 'checkpoint.pth.tar'
path_name = ntpath.basename(opt.test_resume_path)[:-19]
web_dir = os.path.join(opt.results_dir, path_name, '%s_%s' % ('test', start_epoch))

real_video_path = opt.test_real_video_path
test_folder = Test_VideoFolder(root=opt.test_root, video=real_video_path, config=opt)
test_dataloader = DataLoader(test_folder, batch_size=1,
                                shuffle=False, num_workers=1)

model, _, start_epoch = util.load_test_checkpoint(opt.test_resume_path, model)

# test
start = time.time()
for i, data in enumerate(test_dataloader):
    
    # inference during test
    if i<len(test_dataloader):
        model.set_test_input(data)
        model.test_train()
    
    model.set_test_input(data)
    model.test()
    visuals = model.get_current_visuals()
    real_vid_path = model.get_real_vid_paths()
    visualizer.save_images_test(web_dir, visuals, real_vid_path, i, opt.test_num)
end = time.time()
print('finish processing in %03f seconds' % (end - start))
cv2.destroyAllWindows()