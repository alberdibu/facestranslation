#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 18 14:42:14 2019

@author: gatvprojects
"""

from __future__ import print_function, division
import os
import numpy as np
from torch.utils.data import Dataset, DataLoader
import cv2

# Constants
crop_x = 2
crop_y = 2

def calculate_video_block(config, path):
    video_data_length = config.test_audio_video_length
    video_pair = range(0, video_data_length)
    im_pth = []
    video_block = np.zeros((config.test_audio_video_length,
                            config.image_size,
                            config.image_size,
                            config.image_channel_size))
    
    k1 = 0
    for image_num in video_pair:
            image_path = os.path.join(path, str(image_num) + '.jpg')
            im_pth.append(image_path)
            if os.path.exists(image_path):
                image = cv2.resize(cv2.imread(image_path), (256,256))

                image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

                image = image / 255

                video_block[k1] = image
            else:
                print("video_block = 0")
                break

            k1 += 1
            
    return video_block, im_pth

def Test_Outside_Loader(root_path, video_path, config):
    loader = {}
    
    if os.path.isdir(root_path) and os.path.isdir(video_path):

        root_video_block, root_im_pth = calculate_video_block(config, root_path)
        real_video_block, real_im_pth = calculate_video_block(config, video_path)

        root_video_block = root_video_block.transpose((0, 3, 1, 2))
        real_video_block = real_video_block.transpose((0, 3, 1, 2))
        loader['A'] = real_video_block
        loader['B'] = root_video_block
        loader['A_path'] = real_im_pth
        loader['B_path'] = root_im_pth
        
    return loader


class Test_VideoFolder(Dataset):

    def __init__(self, root, video, config, loader=Test_Outside_Loader):
        self.root = root
        self.video = video
        self.config = config
        self.loader = loader

        print(root, video)

        self.vid = self.loader(self.root, self.video,  config=self.config)

    def __getitem__(self, index):
        loader = {}

        loader['A'] = self.vid['A'][index:index + self.config.sequence_length, :, :, :]
        loader['B'] = self.vid['B'][index:index + self.config.sequence_length, :, :, :]
        loader['A_path'] = self.vid['A_path'][index:self.config.sequence_length + index]
        loader['B_path'] = self.vid['B_path'][index:self.config.sequence_length + index]
        return loader

    def __len__(self):
        return self.config.test_audio_video_length - self.config.sequence_length + 1
