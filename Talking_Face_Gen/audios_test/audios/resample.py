import librosa

y, sr = librosa.load("English.wav", sr=44100)
y_8k = librosa.resample(y, sr, 16000)
librosa.output.write_wav('English16K.wav', y_8k, 16000)